## Quick summary
Infra setup for PHP 7.1 + mysql + memcache,  uses docker as a main engine behind the automation.
Before you start setting the infra up make sure you have:
- docker 1.13+ installed
- docker compose
- git installed


#### Installation steps
- clone infrastructure repository and check it out to php7-mysql branch
```
git clone https://tzubertowski@bitbucket.org/mockdoc/mockinfra.git && cd mockinfra && git checkout -b php7-mysql && git pull origin php7-mysql
```
- install docker from [their website](https://www.docker.com)
- install docker compose from [their website](https://docs.docker.com/compose/install/#install-compose)
- create your application and move it to mockinfra/application folder (make the dir if it doesn't exist)
- run 
	```
	docker-compose up
	```
- profit ??

### Details about docker setup
You can find more information about docker and docker compose used in this file [here](DOCKER_README.md).
